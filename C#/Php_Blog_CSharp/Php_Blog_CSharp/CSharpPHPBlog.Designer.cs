﻿namespace Php_Blog_CSharp
{
    partial class CSharpPHPBlog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.userText = new System.Windows.Forms.TextBox();
            this.passText = new System.Windows.Forms.TextBox();
            this.registerLabel = new System.Windows.Forms.LinkLabel();
            this.loginButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // userText
            // 
            this.userText.Location = new System.Drawing.Point(267, 133);
            this.userText.Name = "userText";
            this.userText.Size = new System.Drawing.Size(308, 20);
            this.userText.TabIndex = 0;
            this.userText.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // passText
            // 
            this.passText.Location = new System.Drawing.Point(267, 159);
            this.passText.Name = "passText";
            this.passText.Size = new System.Drawing.Size(308, 20);
            this.passText.TabIndex = 1;
            this.passText.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // registerLabel
            // 
            this.registerLabel.AutoSize = true;
            this.registerLabel.Location = new System.Drawing.Point(267, 186);
            this.registerLabel.Name = "registerLabel";
            this.registerLabel.Size = new System.Drawing.Size(46, 13);
            this.registerLabel.TabIndex = 2;
            this.registerLabel.TabStop = true;
            this.registerLabel.Text = "Register";
            // 
            // loginButton
            // 
            this.loginButton.Location = new System.Drawing.Point(499, 186);
            this.loginButton.Name = "loginButton";
            this.loginButton.Size = new System.Drawing.Size(75, 23);
            this.loginButton.TabIndex = 3;
            this.loginButton.Text = "Login";
            this.loginButton.UseVisualStyleBackColor = true;
            this.loginButton.Click += new System.EventHandler(this.loginButton_Click);
            // 
            // CSharpPHPBlog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(841, 428);
            this.Controls.Add(this.loginButton);
            this.Controls.Add(this.registerLabel);
            this.Controls.Add(this.passText);
            this.Controls.Add(this.userText);
            this.Name = "CSharpPHPBlog";
            this.Text = "CSharpPHPBlog";
            this.Load += new System.EventHandler(this.CSharpPHPBlog_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox userText;
        private System.Windows.Forms.TextBox passText;
        private System.Windows.Forms.LinkLabel registerLabel;
        private System.Windows.Forms.Button loginButton;
    }
}

