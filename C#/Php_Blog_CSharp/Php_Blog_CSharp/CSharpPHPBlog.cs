﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Php_Blog_CSharp
{
    public partial class CSharpPHPBlog : Form
    {
        public CSharpPHPBlog()
        {
            InitializeComponent();
        }

        string Username, Password;
        DataConnection dc = new DataConnection();

        private void CSharpPHPBlog_Load(object sender, EventArgs e)
        {
                              
        }

        private void login_components_hide()
        {
            userText.Hide();
            passText.Hide();
            registerLabel.Hide();
            loginButton.Hide();
        }

        private void loginButton_Click(object sender, EventArgs e)
        {
            Username = userText.Text;
            Password = passText.Text;
            if (dc.validate_login(Username, Password) == true)
            {
                login_components_hide();
            }
        }
    }
}
