<?php
 
$dosya_adi = ($_GET['id']).".php";

function cleanInput($input) {

  $search = array(
    '@<script[^>]*?>.*?</script>@si',   // Javascript kodlar�n� temizleme
    '@<[\/\!]*?[^<>]*?>@si',            // HTML kodlar�n� temizleme
    '@<style[^>]*?>.*?</style>@siU',    // Stil kodlar�n� d�zenleme
    '@<![\s\S]*?--[ \t\n\r]*>@'         // �oklu yorum sat�rlar�n� temizleme
  );

    $output = preg_replace($search, '', $input);
    return $output;
  }

function sanitize($input) {
    if (is_array($input)) {
        foreach($input as $var=>$val) {
            $output[$var] = sanitize($val);
        }
    }
    else {
        if (get_magic_quotes_gpc()) {
            $input = stripslashes($input);
        }
        $input  = cleanInput($input);
        $output = mysql_real_escape_string($input);
    }
    return $output;
}

if (file_exists($dosya_adi))
{
echo "Dosya zaten var!";
$_GET  = sanitize($_GET);
}
else
 
{
touch($dosya_adi);
echo "Dosya olu�turuldu.";
$_GET  = sanitize($_GET);
}
 
?>